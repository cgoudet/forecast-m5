# forecast-m5

Training in time serie forecasting by participating in M5 kaggle competition.

To run to total pipeline:
```
python -m src.data.make_dataset small
```

To run a specified model in `references/models` :
```
python -m src.models.train <model_name>
```

To compile the documentation :
```
cd docs
make html
```
