FROM docker.io/bitnami/spark:3
USER root
ENV HOME=/home/cgoudet
RUN useradd -m cgoudet && export PATH=/home/cgoudet/.local/bin:$PATH
RUN apt-get update && apt-get upgrade -y && apt-get install -y git && \
    rm -r /var/lib/apt/lists /var/cache/apt/archives
ADD requirements.txt /
RUN pip install --no-cache-dir -U pip wheel \
    && pip install --no-cache-dir -r /requirements.txt
USER cgoudet
RUN jupyter nbextension install https://github.com/drillan/jupyter-black/archive/master.zip --user \
    && jupyter nbextension enable jupyter-black-master/jupyter-black
WORKDIR /home/cgoudet
