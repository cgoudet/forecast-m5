import numpy as np
import pandas as pd

from src.features.embeddings import item_snap_shift


def test_item_snap_shift():
    inp = pd.DataFrame(
        {
            "item": [1, 1, 1, 2, 2, 3],
            "qty": [2, 4, 6, 8, 10, 12],
            "snap": [0, 0, 1, 0, 1, np.nan],
            "encoder_rolling_store_item_28d_qty_dayofweek_median": 4,
        }
    )
    mapper = item_snap_shift(inp)
    exp = pd.DataFrame(
        {"item_snap_shift": [-0.75, -0.5]}, index=pd.Index([1, 2], name="item")
    )
    pd.testing.assert_frame_equal(mapper, exp)
