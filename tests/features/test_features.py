from datetime import datetime

import numpy as np
import pandas as pd

from src.features.features import future_features, process_snap, product_features


def test_process_snap():
    calendar = pd.DataFrame(
        {
            "d": ["d0", "d1", "d2"],
            "snap_CA": [1, 0, 0],
            "snap_TX": [0, 0, 1],
            "snap_WI": [0, 1, 0],
            "other": 2,
        }
    )
    sales = pd.DataFrame({"d": ["d0", "d1", "d2"], "whatever": 3, "state": [0, 0, 1]})
    out = process_snap(sales, calendar)
    exp = pd.DataFrame(
        {
            "d": ["d0", "d1", "d2"],
            "whatever": 3,
            "state": [0, 0, 1],
            "snap": np.int8([1, 0, 1]),
        }
    )
    pd.testing.assert_frame_equal(out, exp)


class TestFutureFeatures:
    def test_pandas(self):
        inp = pd.DataFrame(
            {
                "date": [
                    datetime(2021, 1, 1),
                    datetime(2021, 1, 29),
                    datetime(2021, 1, 29),
                ],
                "store": [1, 1, 2],
                "item": [1, 1, 2],
                "qty": [1, 2, 3],
            }
        ).assign(date_f28=lambda df: df["date"] + pd.offsets.DateOffset(days=28))

        out = future_features(inp, ["qty"])

        exp = inp.assign(qty_f28=[2, np.nan, np.nan])
        pd.testing.assert_frame_equal(out, exp)


class TestProductFeatures:
    def test_with_extra_column(self):
        inp = pd.DataFrame(
            {"first": [0, 1, 2], "second": [3, 4, 5], "other": [7, 8, 8]}
        )

        out = product_features(inp, [["first", "second"]])

        exp = inp.assign(product__first__second=[0, 4, 10])
        pd.testing.assert_frame_equal(out, exp)
