import tempfile
from pathlib import Path

import numpy as np
import pandas as pd

from src.features.emb_umap import train_emb_umap


class TestRolling:
    def test_standard(self):
        fn = Path(__file__).parent / "fixtures" / "test_sales.csv"
        inp = pd.read_csv(fn, parse_dates=["date"]).assign(
            week=lambda x: x["date"].dt.to_period("W").astype(int),
            dayofweek=lambda x: x["date"].dt.dayofweek,
            keep_agg=1,
        )

        config = {
            "week_store": {
                "remove_small_clusters": True,
                "add_mean_level": 1,
                "umap_opt": dict(
                    n_neighbors=30, min_dist=0.1, n_components=2, random_state=42
                ),
                "pivot_opt": dict(
                    index=["week", "store"],
                    columns=["dayofweek"],
                    values="qte",
                    aggfunc=np.sum,
                    fill_value=0,
                ),
            }
        }
        with tempfile.TemporaryDirectory() as tempdir:
            tempdir = Path(tempdir)
            train_emb_umap(inp, "unittest", config, tempdir)
            out = pd.read_parquet(tempdir / "encoder_unittest_umap_week_store.parquet")

        exp_fn = Path(__file__).parent / "fixtures" / "output_umap_store_week.csv"
        exp = pd.read_csv(exp_fn, index_col=["week", "store"]).astype(np.float32)
        pd.testing.assert_frame_equal(out, exp)
