from datetime import datetime

import numpy as np
import pandas as pd
from pytest import fixture

from src.features.rolling import (
    dayofweek_mean,
    dayofweek_median,
    dayofweek_std,
    dayofweek_sum,
    naive_error,
    remove_outliers,
    rolling_group,
    zero_rate,
)


class TestRolling:
    def test_lines_are_agregated_by_group(self):
        inp = pd.DataFrame(
            [
                ["2021-01-01", 1],
                ["2021-01-01", 2],
                ["2021-01-02", 4],
                ["2021-01-02", 8],
                ["2021-01-08", 16],
                ["2021-01-08", 32],
            ],
            columns=["consumption_date_period", "qty"],
        ).assign(
            consumption_date_period=lambda df: pd.to_datetime(
                df["consumption_date_period"]
            ),
            tag=0,
        )

        configs = {
            "group": ["tag"],
            "agg": [{"window": "7d", "fcts": ["sum"]}],
        }

        out = rolling_group(inp, configs, "consumption_date_period")

        exp = (
            pd.DataFrame(
                [
                    ["2021-01-02", 15.0],
                    ["2021-01-08", 60],
                ],
                columns=[
                    "consumption_date_period",
                    "encoder_rolling_tag_7d_qty_sum",
                ],
            )
            .assign(
                consumption_date_period=lambda df: pd.to_datetime(
                    df["consumption_date_period"]
                ),
                tag=0,
            )
            .set_index(["consumption_date_period", "tag"])
        )
        assert out[0] == "tag"
        pd.testing.assert_frame_equal(out[1], exp)

    def test_rolling_sum_with_multi_column_group(self):
        inp = pd.DataFrame(
            [
                ["2021-01-01", 1, 0],
                ["2021-01-01", 2, 1],
                ["2021-01-02", 4, 0],
                ["2021-01-02", 8, 1],
                ["2021-01-08", 16, 0],
                ["2021-01-08", 32, 1],
            ],
            columns=["consumption_date_period", "qty", "company"],
        ).assign(
            consumption_date_period=lambda df: pd.to_datetime(
                df["consumption_date_period"]
            ),
            tag=0,
        )

        configs = {
            "group": ["company", "tag"],
            "agg": [{"window": "7d", "fcts": ["sum"]}],
        }

        out = rolling_group(inp, configs, "consumption_date_period")

        exp = (
            pd.DataFrame(
                [
                    ["2021-01-02", 0, 5.0],
                    ["2021-01-02", 1, 10.0],
                    ["2021-01-08", 0, 20],
                    ["2021-01-08", 1, 40],
                ],
                columns=[
                    "consumption_date_period",
                    "company",
                    "encoder_rolling_company_tag_7d_qty_sum",
                ],
            )
            .assign(
                consumption_date_period=lambda df: pd.to_datetime(
                    df["consumption_date_period"]
                ),
                tag=0,
            )
            .set_index(["consumption_date_period", "company", "tag"])
        )
        assert out[0] == "company_tag"
        pd.testing.assert_frame_equal(out[1], exp)

    def test_underscore_agregations_are_ignored(self):
        inp = pd.DataFrame(
            [
                ["2021-01-01", 1],
                ["2021-01-02", 8],
                ["2021-01-08", 32],
            ],
            columns=["consumption_date_period", "qty"],
        ).assign(
            consumption_date_period=lambda df: pd.to_datetime(
                df["consumption_date_period"]
            ),
            tag=0,
        )

        configs = {
            "group": ["tag"],
            "agg": [{"window": "7d", "fcts": ["sum", "_dayofweek_median"]}],
        }

        out = rolling_group(inp, configs, "consumption_date_period")

        exp = (
            pd.DataFrame(
                [
                    ["2021-01-02", 9.0],
                    ["2021-01-08", 40],
                ],
                columns=[
                    "consumption_date_period",
                    "encoder_rolling_tag_7d_qty_sum",
                ],
            )
            .assign(
                consumption_date_period=lambda df: pd.to_datetime(
                    df["consumption_date_period"]
                ),
                tag=0,
            )
            .set_index(["consumption_date_period", "tag"])
        )
        assert out[0] == "tag"
        pd.testing.assert_frame_equal(out[1], exp)

    def test_empty_group_agregation(self):
        inp = pd.DataFrame(
            [
                ["2021-01-01", 1],
                ["2021-01-02", 8],
                ["2021-01-08", 32],
            ],
            columns=["consumption_date_period", "qty"],
        ).assign(
            consumption_date_period=lambda df: pd.to_datetime(
                df["consumption_date_period"]
            ),
            tag=0,
        )

        configs = {
            "group": ["tag"],
            "agg": [{"window": "7d", "fcts": []}],
        }

        out = rolling_group(inp, configs, "consumption_date_period")
        assert out[0] == "tag"
        assert out[1].empty

    def test_multiple_windows_in_same_group(self):
        inp = pd.DataFrame(
            [
                [datetime(2021, 1, 1), 2],
                [datetime(2021, 1, 2), 4],
                [datetime(2021, 1, 8), 16],
            ],
            columns=["date", "qty"],
        ).assign(tag=0)

        configs = {
            "group": ["tag"],
            "agg": [
                {"window": "7d", "fcts": ["sum"]},
                {"window": "56d", "fcts": ["sum"]},
            ],
        }

        out = rolling_group(inp, configs, "date")

        exp = (
            pd.DataFrame(
                [
                    [datetime(2021, 1, 2), 6, 6],
                    [datetime(2021, 1, 8), 22.0, 20.0],
                ],
                columns=[
                    "date",
                    "encoder_rolling_tag_56d_qty_sum",
                    "encoder_rolling_tag_7d_qty_sum",
                ],
            )
            .assign(tag=0)
            .set_index(["date", "tag"])
        )
        assert out[0] == "tag"
        pd.testing.assert_frame_equal(out[1], exp)

    def test_with_other_target(self):
        inp = pd.DataFrame(
            {
                "consumption_date_period": pd.date_range("2021-01-01", "2021-01-08"),
                "quantity": np.arange(8),
                "quantity2": 2 * np.arange(8),
                "tag": 0,
            }
        )
        configs = {
            "group": ["tag"],
            "agg": [
                {
                    "window": "56d",
                    "fcts": ["dayofweek_median"],
                    "targets": ["quantity", "quantity2"],
                }
            ],
        }

        out = rolling_group(inp, configs, "consumption_date_period")

        exp = pd.DataFrame(
            [
                [datetime(2021, 1, 8), 7.0, 3.5, 0],
            ],
            columns=[
                "consumption_date_period",
                "encoder_rolling_tag_56d_quantity2_dayofweek_median",
                "encoder_rolling_tag_56d_quantity_dayofweek_median",
                "tag",
            ],
        ).set_index(["consumption_date_period", "tag"])
        assert out[0] == "tag"
        pd.testing.assert_frame_equal(out[1], exp)

    def test_not_consider_starting_nan(self):
        inp = pd.DataFrame(
            {
                "date": list(pd.date_range("2021-01-01", "2021-01-08")) * 2,
                "company": [0] * 8 + [1] * 8,
                "qty": [np.nan] * 2 + list(np.arange(6)) + list(np.arange(8)),
            }
        ).dropna()
        config = {
            "group": ["company"],
            "agg": [{"window": "56d", "fcts": ["dayofweek_median"]}],
        }
        out = rolling_group(inp, config, "date")
        exp = pd.DataFrame(
            [
                [datetime(2021, 1, 8), 3.5, 1],
            ],
            columns=[
                "date",
                "encoder_rolling_company_56d_qty_dayofweek_median",
                "company",
            ],
        ).set_index(["date", "company"])
        assert out[0] == "company"
        pd.testing.assert_frame_equal(out[1], exp)


class TestDayofWeekAgg:
    @fixture(autouse=True)
    def intermediate_consumptions(self):
        return list(np.arange(7 - 1))

    def test_std_no_nan(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions + [2]
        assert dayofweek_std(inp, 2) == 1 / 2

    def test_std_small(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions
        assert np.isnan(dayofweek_std(inp, 2))

    def test_std_nan(self, intermediate_consumptions):
        inp = (
            [1] + intermediate_consumptions + [np.nan] + intermediate_consumptions + [3]
        )
        assert dayofweek_std(inp, 2) == 1

    def test_mean_no_nan(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions + [2]
        assert dayofweek_mean(inp, 2) == 1.5

    def test_mean_small(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions
        assert np.isnan(dayofweek_mean(inp, 2))

    def test_mean_nan(self, intermediate_consumptions):
        inp = (
            [1] + intermediate_consumptions + [np.nan] + intermediate_consumptions + [3]
        )
        assert dayofweek_mean(inp, 2) == 2

    def test_median_no_nan(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions + [2] + intermediate_consumptions + [3]
        assert dayofweek_median(inp, 2) == 2

    def test_median_small(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions
        assert np.isnan(dayofweek_median(inp, 2))

    def test_median_nan(self, intermediate_consumptions):
        inp = (
            [1] + intermediate_consumptions + [np.nan] + intermediate_consumptions + [3]
        )
        assert dayofweek_median(inp, 2) == 2

    def test_zero_rate_no_nan(self):
        inp = np.array([0, 1, 0, 2])
        assert zero_rate(inp) == 0.5

    def test_zero_rate_nan(
        self,
    ):
        inp = np.array([0, 1, 0, 2, np.nan, np.nan])
        assert zero_rate(inp) == 0.5

    def test_naive_no_nan(self):
        inp = np.array([0, 1, 0, 2])
        assert naive_error(inp) == np.sqrt(2)

    def test_naive_nan(self):
        inp = np.array([0, 1, 0, 2, np.nan, np.nan])
        assert naive_error(inp) == np.sqrt(2)

    def test_sum_nan(self, intermediate_consumptions):
        inp = (
            [1] + intermediate_consumptions + [np.nan] + intermediate_consumptions + [3]
        )
        assert dayofweek_sum(inp, 3) == 4

    def test_sum_small(self, intermediate_consumptions):
        inp = [1] + intermediate_consumptions
        assert np.isnan(dayofweek_sum(inp, 3))


class TestRemoveOutliers:
    def test_without_keep_agg_remains_same(self):
        inp = pd.DataFrame({"quantity": [0, 1, 2]})
        out = remove_outliers(inp, ["quantity"])
        pd.testing.assert_frame_equal(out, inp)

    def test_with_keep_agg_replace_with_nan_limited_to_targets(self):
        inp = pd.DataFrame(
            {
                "quantity": [0, 1, 2],
                "quantity2": [2, 3, 4],
                "quantity3": [5, 6, 7],
                "keep_agg": [1, 0, 1],
            }
        )
        out = remove_outliers(inp, ["quantity", "quantity2"])

        exp = pd.DataFrame(
            {
                "quantity": [0, np.nan, 2],
                "quantity2": [2, np.nan, 4],
                "quantity3": [5, 6, 7],
                "keep_agg": [1, 0, 1],
            }
        )
        pd.testing.assert_frame_equal(out, exp)
