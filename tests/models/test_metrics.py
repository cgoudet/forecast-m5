import numpy as np
import pandas as pd
import pytest

from src.models.metrics import WRMSSEEvaluator, bias, compute_metrics, mase, rmse, urmse


def test_rmse():
    preds = [1, 2, 3]
    target = [4, 5, 6]
    np.testing.assert_allclose(rmse(target, preds), 3)


def test_unweighted_bias():
    preds = [1, 2, 3]
    target = [-1, -2, 7]
    np.testing.assert_allclose(bias(target, preds), -2)


def test_weighted_bias():
    preds = [1, 2, 3]
    target = [-1, -2, 7]
    weights = np.array([1, 3, 5])
    np.testing.assert_allclose(bias(target, preds, weights), 6)


def test_compute_metrics():
    inp = pd.DataFrame({"target": [-1, -2, 7], "pred": [1, 2, 3]})
    out = compute_metrics(
        inp, "target", "pred", [("mean_absolute_error", None), ("bias", None)]
    )
    assert out == {"mean_absolute_error": 10 / 3, "bias": -2}


def test_compute_metrics_with_weight():
    inp = pd.DataFrame({"target": [-1, -2, 7], "pred": [1, 2, 3], "weight": [3, 2, 1]})
    out = compute_metrics(inp, "target", "pred", [("mean_absolute_error", "weight")])
    assert out == {"mean_absolute_error": 3}


def test_compute_unknown_metrics():
    inp = pd.DataFrame({"target": [-1, -2, 7], "pred": [1, 2, 3]})
    with pytest.raises(KeyError) as exc:
        compute_metrics(inp, "target", "pred", [("whatever", None)])
    assert str(exc.value) == "'Unknown metric : whatever'"


def test_urmse_with_weight():
    truth = [1, 2, 3, 4, 5]
    preds = [2, 3, 4, 5, 6]
    weights = [1, 1, 2, 2, 3]
    assert urmse(truth, preds, sample_weight=weights) == np.sqrt(9 / 5)


def test_urmse_without_weight():
    truth = [1, 2, 3, 4, 5]
    preds = [2, 3, 4, 5, 6]
    assert urmse(truth, preds) == 1


def test_mase_with_weight():
    truth = [1, 2, 3, 4, 5]
    preds = [2, 3, 4, 5, 6]
    weights = [1, 1, 2, 2, 3]
    assert mase(truth, preds, sample_weight=weights) == 9 / 5


def test_mase_without_weight():
    truth = [1, 2, 3, 4, 5]
    preds = [2, 3, 4, 5, 6]
    assert mase(truth, preds) == 1


def test_WRMSSEEvaluator():
    dt_range = pd.date_range("2021-01-01", "2021-02-28", freq="d")
    cat0 = pd.DataFrame(
        {"date": dt_range, "qty_f28": dt_range.dayofyear % 2, "est_qty": 0.5, "cat": 0}
    )
    cat1 = pd.DataFrame(
        {
            "date": dt_range,
            "qty_f28": 2 * (dt_range.dayofyear % 2),
            "est_qty": 2,
            "cat": 1,
        }
    )
    df = pd.concat([cat0, cat1], axis=0).assign(
        ca=lambda x: x["qty_f28"] * (x["cat"] + 2)
    )
    split = df.date >= "2021-02-01"
    train, valid = df[~split], df[split]
    evaluator = WRMSSEEvaluator(groups=["cat"], horizon=28).fit(train)
    assert pytest.approx(evaluator.score(valid), 1e-7) == 0.61757013
