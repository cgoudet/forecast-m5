import pytest

from src.data.pipeline_spark import spark_context


@pytest.fixture(scope="session")
def context():
    return spark_context()
