import pandas as pd

from src.data.quality import drop_bad_quality


class TestDropBadQuality:
    def test_drop_0_1394_peaks(self):
        inp = pd.DataFrame(
            [[0, 1394, 100], [0, 1394, 10]], columns=["store", "item", "qte"]
        )
        out = drop_bad_quality(inp)
        # exp = inp.iloc[[1]]
        pd.testing.assert_frame_equal(out, inp)
