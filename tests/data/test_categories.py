import numpy as np
import pandas as pd

from src.data.categories import (
    Cat,
    Dept,
    Item,
    State,
    Store,
    categorify_ids,
    uncategorify_ids,
)


class TestState:
    def test_pandas(self):
        serie = pd.Series(["CA", "WI"])
        st = State(pipeline="pandas")

        ints = st.encode(serie)
        exp = pd.Series(np.int8([0, 2]))
        pd.testing.assert_series_equal(ints, exp)
        pd.testing.assert_series_equal(st.decode(ints), serie)


class TestStore:
    def test_pandas(self):
        serie = pd.Series(["CA_1", "WI_2"])
        enc = Store(pipeline="pandas")

        ints = enc.encode(serie)
        exp = pd.Series(np.int8([1, 22]))
        pd.testing.assert_series_equal(ints, exp)
        pd.testing.assert_series_equal(enc.decode(ints), serie)


class TestCat:
    def test_pandas(self):
        serie = pd.Series(["HOBBIES", "FOODS"])
        enc = Cat(pipeline="pandas")

        ints = enc.encode(serie)
        exp = pd.Series(np.int8([1, 0]))
        pd.testing.assert_series_equal(ints, exp)
        pd.testing.assert_series_equal(enc.decode(ints), serie)


class TestDept:
    def test_pandas(self):
        serie = pd.Series(["HOBBIES_1", "FOODS_5"])
        enc = Dept(pipeline="pandas")

        ints = enc.encode(serie)
        exp = pd.Series(np.int8([11, 5]))
        pd.testing.assert_series_equal(ints, exp)
        pd.testing.assert_series_equal(enc.decode(ints), serie)


class TestItem:
    def test_pandas(self):
        serie = pd.Series(["HOBBIES_1_001", "FOODS_5_001"])
        enc = Item(pipeline="pandas")

        ints = enc.encode(serie)
        exp = pd.Series(np.int16([11001, 5001]))
        pd.testing.assert_series_equal(ints, exp)
        pd.testing.assert_series_equal(enc.decode(ints), serie)


class TestCategorifyIds:
    def test_pandas(self):
        inp = pd.DataFrame(
            {
                "item": ["HOBBIES_1_001", "FOODS_5_001"],
                "dept": ["HOBBIES_1", "FOODS_5"],
                "cat": ["HOBBIES", "FOODS"],
                "store": ["CA_1", "WI_2"],
                "state": ["CA", "WI"],
            }
        )
        out = categorify_ids(inp, "pandas")
        exp = pd.DataFrame(
            {
                "item": np.int16([11001, 5001]),
                "dept": np.int8([11, 5]),
                "cat": np.int8([1, 0]),
                "store": np.int8([1, 22]),
                "state": np.int8([0, 2]),
            }
        )
        pd.testing.assert_frame_equal(out, exp)

        pd.testing.assert_frame_equal(uncategorify_ids(out, "pandas"), inp)
