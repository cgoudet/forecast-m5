import pandas as pd
import pytest

from src.data.pipeline_spark import melt


@pytest.mark.skip()
def test_merge_with_auto_value_vars(context):
    inp = pd.DataFrame({"id0": [0, 1], "id1": [2, 3], "d0": 1, "d1": 1})

    out = melt(
        context.createDataFrame(inp),
        id_vars=["id0", "id1"],
        var_name="date",
        value_name="qty",
    ).toPandas()
    exp = (
        inp.melt(id_vars=["id0", "id1"], var_name="date", value_name="qty")
        .sort_values(["id0", "id1"])
        .reset_index(drop=True)
    )
    pd.testing.assert_frame_equal(out, exp)
