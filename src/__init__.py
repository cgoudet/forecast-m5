import logging
from pathlib import Path

import matplotlib as mpl
from dotenv import load_dotenv


def _datadir(rootdir):
    fn = Path("/data/datasets")
    return fn if fn.exists() else rootdir / "data/datasets"


ROOTDIR = Path(__file__).parent / ".."
DATADIR = _datadir(ROOTDIR)
ENCODIR = ROOTDIR / "data" / "encoders"
SUBMITDIR = ROOTDIR / "data" / "submissions"
MODELSDIR = ROOTDIR / "data" / "models"
logging.basicConfig(level=logging.INFO)
mpl.rcParams["axes.spines.top"] = False
mpl.rcParams["axes.spines.right"] = False
mpl.rcParams["text.usetex"] = False

load_dotenv()
