from os import getenv


class Config:
    FUTURE_FEATURES = ["qty", "snap"]

    SOURCE_PIPELINE_VALIDATION = "pandas"

    PROCESSING_ROLLING_MIN_PERIODS = 2
    ROLLING_CONFIG = [
        {
            "group": ["store", "item"],
            "agg": [
                {
                    "window": "63d",
                    "fcts": ["dayofweek_median"],
                },
                {
                    "window": "28d",
                    "fcts": ["dayofweek_median", "sum"],
                    "targets": ["qty", "ca"],
                    "min_periods": 28,
                    "fillna": {"method": "bfill"},
                },
                {
                    "window": "expand",
                    "fcts": ["naive_error"],
                    "min_periods": 28,
                    "fillna": {"method": "bfill"},
                },
            ],
        }
    ]
    KFOLD = 5
    NEPTUNE_TOKEN = getenv("NEPTUNE_TOKEN")
    NEPTUNE_USER = getenv("NEPTUNE_USER")
