from dataclasses import dataclass

import numpy as np
import pandas as pd

from src import ENCODIR


@dataclass
class Embedding:
    fct: callable
    params: dict = None


def learn_merge_embedding(
    sales: pd.DataFrame, embedding: Embedding, source: str, force: bool, **kwargs
) -> pd.DataFrame:
    """
    Learn and merge directly an embedding or read it from disk
    """
    fn = ENCODIR / f"{source}_{embedding.fct.__name__}.parquet"
    if not fn.exists() or force:
        mapper = embedding.fct(sales, **(embedding.params or {}))
    else:
        mapper = pd.read_parquet(fn)
    on = (
        mapper.index.names
        if isinstance(mapper.index, pd.MultiIndex)
        else mapper.index.name
    )
    return pd.merge(sales, mapper, on=on, how="left")


def item_snap_shift(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Create an item embedding based on the bias shift of a naive model due to snap.
    """
    return (
        sales.dropna(subset=["snap"])
        .astype({"snap": np.int8})
        .assign(
            diff=lambda df: 1
            - df["qty"].div(
                df["encoder_rolling_store_item_28d_qty_dayofweek_median"].clip(1)
            )
        )
        .pivot_table(index=["item"], columns="snap", values="diff", aggfunc="mean")
        .assign(item_snap_shift=lambda df: df[1] - df[0])[["item_snap_shift"]]
        .rename_axis(None, axis=1)
    )
