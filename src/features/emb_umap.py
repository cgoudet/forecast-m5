import logging
from collections import Counter
from pathlib import Path

import pandas as pd
from sklearn.cluster import DBSCAN
from umap import UMAP


def train_emb_umap(
    sales: pd.DataFrame,
    source: str,
    config: dict,
    outdir: Path,
    force: bool = False,
    **kwargs,
):
    """
    Train all UMAP embeddings defined by the config dict.
    """
    reference = sales[sales["keep_agg"] == 1]
    for name, opts in config.items():
        if name.startswith("_"):
            continue
        encoder_fn = outdir / f"encoder_{source}_umap_{name}.parquet"
        logging.info({"fct": "train_emb_umap", "encoder": encoder_fn.stem})
        if encoder_fn.exists() and not force:
            continue

        mapper = emb_umap(
            reference, name=name, pivot_opt=opts["pivot_opt"], umap_opt=opts["umap_opt"]
        )
        if opts.get("add_mean_level") is not None:
            mapper = _add_mean_level(
                mapper,
                name=name,
                level=opts["add_mean_level"],
                remove_small_clusters=opts.get("remove_small_clusters", False),
            )
        mapper.to_parquet(encoder_fn)


def emb_umap(
    sales: pd.DataFrame, name: str, pivot_opt: dict, umap_opt: dict
) -> pd.DataFrame:
    """
    Create embeddings based on UMAP dimension reduction.

    The embedding is created by first running a pivot table on the DataFrame with the
    provided parameters, and normalizing over the lines.
    Then UMAP is applied to this table with the provided parameters.

    :param sales:
    DataFrame with the raw data.

    :param pivot_opt:
    Options that will be passed to the pivot_table function.

    :param umap_opt:
    Options that will be passed to the umap options.

    :return DataFrame:
    Embeddings.
    """

    store_weekly_distrib = _store_weekly_distrib(sales, pivot_opt)
    return _weekly_distrib_embeddings(store_weekly_distrib, name, umap_opt)


def _store_weekly_distrib(sales: pd.DataFrame, pivot_opt) -> pd.DataFrame:

    store_weekly_distrib = sales.pivot_table(**pivot_opt)

    total_per_store_week = store_weekly_distrib.sum(axis=1)
    store_weekly_distrib = store_weekly_distrib.div(total_per_store_week, axis=0)

    return store_weekly_distrib


def _weekly_distrib_embeddings(
    df: pd.DataFrame, name: str, umap_opt: dict
) -> pd.DataFrame:
    red = UMAP(**umap_opt)
    mapper = pd.DataFrame(
        red.fit_transform(df),
        index=df.index,
        columns=[f"encoder_umap_{name}_{i}" for i in range(red.n_components)],
    )
    return mapper


def _add_mean_level(
    embeddings: pd.DataFrame,
    name: str,
    level: int,
    remove_small_clusters=False,
) -> pd.DataFrame:
    """
    Remove outliers and create per store and per store/week embeddings mapper.

    :param embeddings:
    Raw embeddings at all levels

    :param level:
    Levels at which take the mean of the embeddings

    :param remove_small_clusters:
    Wether to remove all but the largest cluster of points.
    All removed points will be replaced by the average.
    """
    renames = {c: c.replace(name, name + "_mean") for c in embeddings.columns}
    reference = _keep_for_mean(embeddings, remove_small_clusters)
    mean_standard_emb = (
        reference.groupby(embeddings.index.names[level])
        .mean()
        .align(embeddings)[0]
        .rename(columns=renames)
    )

    mapper = pd.concat([reference, mean_standard_emb], axis=1).fillna(
        {k: mean_standard_emb[v] for k, v in renames.items()}
    )
    return mapper


def _keep_for_mean(embeddings, remove_small_clusters=False) -> pd.DataFrame:
    if not remove_small_clusters:
        return embeddings
    clt = DBSCAN(eps=1, min_samples=10)
    labels = clt.fit_predict(embeddings)
    top_label = Counter(labels).most_common(1)[0][0]
    return embeddings[labels == top_label]


def _vars_name(df: pd.DataFrame) -> str:
    "Return the string concatenation of index names, separated by _"
    return "_".join(
        df.index.names if isinstance(df.index, pd.MultiIndex) else [df.index.name]
    )
