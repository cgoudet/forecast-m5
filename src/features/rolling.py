import itertools
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pd
import structlog

from src.config import Config

logger = structlog.get_logger()


def dayofweek_mean(x: np.ndarray, min_periods: int) -> np.ndarray:
    """
    Computes the mean for the same period at each week, limiting to a specific number of numbers.
    The aount of numbers to skip depends on the periods in the dataframe.
    """
    first_index = np.argmax(1 - np.isnan(x))
    x = x[first_index:]
    step = 7
    if len(x) <= step * (min_periods - 1):
        return np.nan
    return np.nanmean(x[-1::-step])


def dayofweek_sum(x: np.ndarray, min_periods: int) -> np.ndarray:
    """
    Computes the sum for the same period at each week, limiting to a specific number of numbers.
    The aount of numbers to skip depends on the periods in the dataframe.
    """
    first_index = np.argmax(1 - np.isnan(x))
    x = x[first_index:]
    step = 7
    if len(x) <= step * (min_periods - 1):
        return np.nan
    return np.nansum(x[-1::-step])


def dayofweek_median(x: np.ndarray, min_periods: int) -> np.ndarray:
    """
    Computes the median for the same period at each week, limiting to a specific number of numbers.
    The aount of numbers to skip depends on the periods in the dataframe.
    """
    first_index = np.argmax(1 - np.isnan(x))
    x = x[first_index:]
    step = 7
    if len(x) <= step * (min_periods - 1):
        return np.nan
    return np.nanmedian(x[-1::-step])


def dayofweek_std(x: np.ndarray, min_periods: int) -> np.ndarray:
    """
    Computes the std for the same period at each week, limiting to a specific number of numbers.
    The aount of numbers to skip depends on the periods in the dataframe.
    """
    first_index = np.argmax(1 - np.isnan(x))
    x = x[first_index:]
    step = 7
    if len(x) <= step * (min_periods - 1):
        return np.nan
    return np.nanstd(x[-1::-step])


def zero_rate(x: np.ndarray) -> np.ndarray:
    """
    Computes the rate of non zero values.
    """
    return np.mean(x[~np.isnan(x)] == 0)


def naive_error(x: np.ndarray) -> np.ndarray:
    """
    Computes the root mean squared error (RMSE) of a naive predictor,
    where a point is prediction as the previous point value.
    """
    return np.sqrt(np.nanmean(np.float_power(x[:-1] - x[1:], 2)))


_CUSTOM_FCT = {
    "dayofweek_mean": dayofweek_mean,
    "dayofweek_sum": dayofweek_sum,
    "dayofweek_median": dayofweek_median,
    "dayofweek_std": dayofweek_std,
    "zero_rate": zero_rate,
    "naive_error": naive_error,
}


def rolling_features(
    sales: pd.DataFrame,
    configs: dict[str, Any],
    rolling_reference: str,
    force: bool,
    savedir: Path,
    source: str,
) -> dict[str, pd.DataFrame]:
    """
    Compute agregates of past values consumptions of specific data subgroups.

    :param df:
    Dataframe to compute agregates on. The agregated quantity is the column quantity.

    :param configs:
    A hierarchical dict with the definition of the various agregations to compute.
    """
    targets = _get_target_features(configs)
    all_features = sorted(
        set(_get_config_features(configs) + [rolling_reference, "keep_agg"] + targets)
        & set(sales.columns)
    )
    to_train = sales[all_features].pipe(remove_outliers, targets)
    for group_config in configs:
        group_name = "_".join(group_config["group"])
        outfn = savedir / f"encodings_{source}_rolling_{group_name}.parquet"
        if not outfn.exists() or force:
            _, mapper = rolling_group(to_train, group_config, rolling_reference)
            encodings = encode(sales, mapper)
            encodings.to_parquet(outfn)


def rolling_group(
    df: pd.DataFrame, config: dict[str, Any], rolling_reference: str
) -> pd.DataFrame:
    """
    Run the actual agregation for a specific group of data points.
    """
    group_name = "_".join(config["group"])
    logger.info("rolling_group", group=config["group"])
    targets = _get_target_features([config])
    per_serie = df.pivot_table(
        index=rolling_reference,
        columns=config["group"],
        values=targets,
        aggfunc=["sum", "mean"],
    ).pipe(lambda df: df["sum"].where(df["mean"].notnull()))
    per_serie.columns.names = ["target"] + config["group"]
    stats = []

    for agg_config in config["agg"]:
        logger.info("rolling_group_window", **agg_config)

        local_targets = agg_config.get("targets", ["qty"])
        min_periods = agg_config.get("min_periods", 2)

        windowed_serie = (
            per_serie[local_targets].expanding(min_periods=min_periods)
            if agg_config["window"] == "expand"
            else per_serie[local_targets].rolling(
                window=agg_config["window"],
                min_periods=Config.PROCESSING_ROLLING_MIN_PERIODS,
            )
        )

        for fct in agg_config["fcts"]:
            if fct.startswith("_"):
                continue
            fct = _CUSTOM_FCT.get(fct, fct)
            if isinstance(fct, str):
                fct_name = fct
                res = getattr(windowed_serie, fct)()
            else:
                fct_name = fct.__name__
                args = (
                    [Config.PROCESSING_ROLLING_MIN_PERIODS]
                    if "dayofweek" in fct_name
                    else []
                )
                res = windowed_serie.apply(fct, raw=True, engine="numba", args=args)

            fillna = agg_config.get("fillna", None)
            if fillna:
                res = res.fillna(**fillna)

            res.columns = res.columns.set_levels(
                [
                    f"encoder_rolling_{group_name}_{agg_config['window']}_{x}_{fct_name}"
                    for x in res.columns.levels[0]
                ],
                level=0,
            )
            stats.append(res)
    if not stats:
        return group_name, pd.DataFrame()

    mapper = (
        pd.concat(stats, axis=1)
        .stack(list(range(1, 1 + len(config["group"]))))
        .rename_axis(None, axis=1)
    )
    return group_name, mapper


def remove_outliers(df: pd.DataFrame, targets: list[str]) -> pd.DataFrame:
    """
    Some data points should not be included in rolling agregate because they do not
    represent a standard behavior.
    The target values are replaced with nan.
    Those data points should not be removed as there is a risk to remove entirely some dates;
    which would break dayofweek* functions.
    """
    if "keep_agg" not in df.columns:
        return df
    col_mask = (
        np.array([c in targets for c in df.columns]).reshape(1, -1)
        * (df[["keep_agg"]].values == 0)
    ).astype(bool)
    return df.where(~col_mask)


def _get_config_features(configs: list[dict]) -> list[str]:
    """
    Agregate the list of all features needed to compute all the rolling agregates.
    Duplicates are removed.
    """
    return sorted(
        set(itertools.chain.from_iterable([config["group"] for config in configs]))
    )


def _get_target_features(configs: list[dict]) -> list[str]:
    """
    Agregate the list of all agregated features.
    Duplicates are removed.
    """
    return sorted(
        set(
            itertools.chain.from_iterable(
                [
                    window.get("targets", ["qty"])
                    for config in configs
                    for window in config["agg"]
                ]
            )
        )
    )


def encode(sales: pd.DataFrame, encoder: pd.DataFrame) -> pd.DataFrame:
    """
    Generate encodings and consumptions data from encoder
    """
    keys = encoder.index.names
    encodings = pd.merge(
        sales[keys], encoder, left_on=keys, right_index=True, how="left"
    ).drop(columns=keys)
    return encodings
