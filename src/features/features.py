import json
from collections.abc import Iterable

import numpy as np
import pandas as pd
from cgoudetcore.mappers import Loggifier
from cgoudetcore.utils import reduce_float_type, reduce_int_type

from src.config import Config
from src.data.categories import categorify_ids

from .. import DATADIR, ENCODIR

ZERO_DAYS = [
    pd.Timestamp(f"{y}-{day}") for y in range(2011, 2018) for day in ["12-25", "01-22"]
]


def optimize_ints(sales):
    fn = ENCODIR / "int_dtypes.json"
    if not fn.exists():
        sales = reduce_int_type(sales)
        int_cols = list(sales.select_dtypes("integer").columns)
        dtypes = sales[int_cols].dtypes.astype(str).to_dict()
        with fn.open("w") as f:
            json.dump(dtypes, f)

    with fn.open() as f:
        dtypes = json.load(f)
    return sales.astype(dtypes)


def add_keep_for_agg(sales):
    """
    Define a feature (keep_agg) use to decide if the entry should be incoporated
    in the computation of agregations
    """
    keep_agg = (sales["date"] >= sales["start_date"]) & ~sales["date_f28"].isin(
        ZERO_DAYS
    )
    return sales.assign(keep_agg=lambda df: np.int8(df.get("keep_agg", 1) * keep_agg))


def add_start_date(sales, source):
    fn = ENCODIR / "start_dates.parquet"
    encodings = pd.read_parquet(fn)
    return pd.concat([sales, encodings], axis=1)


def compute_start_date(sales):
    start_date = (
        sales[sales.qte > 0].groupby(["store", "item"]).agg({"date": [np.min, np.max]})
    )
    start_date.columns = ["start_date", "end_date"]
    fn = ENCODIR / "start_dates.parquet"
    start_date.to_parquet(fn)
    return start_date


def add_datepart(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add minimal date dependent features.
    """
    return df.assign(
        dayofweek=np.int8(df["date"].dt.dayofweek),
        month=np.int8(df["date"].dt.month),
        day=np.int8(df["date"].dt.day),
        dayofyear=np.int16(df["date"].dt.dayofyear),
        cos_dayinyear=np.cos(
            2 * np.pi * df["date"].dt.dayofyear / (365 + df["date"].dt.is_leap_year)
        ),
        sin_dayinyear=np.sin(
            2 * np.pi * df["date"].dt.dayofyear / (365 + df["date"].dt.is_leap_year)
        ),
        week=df["date"].dt.to_period("W").astype(np.int16),
    )


def add_log_features(sales: pd.DataFrame, features: list[str]) -> pd.DataFrame:
    """
    Make a log transformation to provided features.
    """
    loggifier = Loggifier(columns=features)
    logs = loggifier.fit_transform(sales)

    opts = dict(columns=["log_" + c for c in features], index=sales.index)
    logs = pd.DataFrame(logs, **opts)
    return pd.concat([sales, logs], axis="columns")


def log_features(sales: pd.DataFrame) -> list[str]:
    """
    Identify features that must be logged.
    """
    features = [
        "encoder_rolling_store_item_28d_qty_dayofweek_median",
        "encoder_rolling_store_item_63d_qty_dayofweek_median",
        "encoder_rolling_store_item_28d_qty_sum",
        "qty",
    ]
    return features


def adapt_types(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Reduce column types to the minimal viable type.
    """
    return sales.pipe(optimize_ints).pipe(reduce_float_type)


def combined_features(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Combine existing features from different steps into new ones.
    """
    products = [
        # ["snap_f28", "log_encoder_rolling_store_item_28d_qty_dayofweek_median"],
        # ["snap_f28", "log_encoder_rolling_store_item_28d_qty_sum"],
    ]
    return (
        sales.pipe(add_log_features, log_features(sales)).pipe(
            product_features, products
        )
        # .pipe(_combine_snap_bias)
        .assign(
            sample_weight=lambda x: x["encoder_rolling_store_item_28d_ca_sum"].div(
                sales["encoder_rolling_store_item_expand_qty_naive_error"].clip(
                    lower=0.1
                )
            ),
            # item_snap=lambda df: np.where(df["snap_f28"], 1, -1) * df["item"],
        )
    )


def _combine_snap_bias(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Create a feature that contains either snap bias shift of a default value if no snap this day.
    """
    M = sales["item_snap_shift"].max()
    m = sales["item_snap_shift"].min()
    shift = (M - m) / 20
    return sales.assign(
        item_snap_flag_shift=lambda df: df["item_snap_shift"].where(
            df["snap_f28"] == 1, M + shift
        )
    )


def drop_no_trainable(sales):
    lagged_features = [x for x in sales.columns if "lagged_encoder" in x]

    mask = ~sales.date.isin(ZERO_DAYS) & (sales.date >= sales.start_date)
    sales = sales[mask].dropna(subset=lagged_features)
    return sales


def merge_prices(sales: pd.DataFrame, context=None):
    """
    Combine data to weekly product prices
    """
    prices = pd.read_parquet(DATADIR / "weekly_prices.parquet")
    return (
        sales.merge(prices, on=["item", "store", "wm_yr_wk"], how="left")
        .merge(
            prices.rename(columns={"price": "price_f28", "wm_yr_wk": "wm_yr_wk_f28"}),
            on=["item", "store", "wm_yr_wk_f28"],
            how="left",
        )
        .fillna({"price": 0, "price_f28": 0})
    )


def merge_date_infos(sales, context=None):
    """
    Add the infos related to specific days from calendar.csv
    """
    calendar_fn = DATADIR / "calendar.csv"
    dtypes = {"wm_yr_wk": np.int16}
    calendar = pd.read_csv(calendar_fn, dtype=dtypes, parse_dates=["date"])
    return (
        sales.pipe(process_date, calendar)
        .pipe(process_snap, calendar)
        .drop(columns="d")
    )


def process_date(sales: pd.DataFrame, calendar: pd.DataFrame) -> pd.DataFrame:
    keep_columns = ["date", "wm_yr_wk", "d"]
    return pd.merge(sales, calendar[keep_columns], on="d")


def process_snap(sales: pd.DataFrame, calendar: pd.DataFrame) -> pd.DataFrame:
    """
    Compress snap columns into a single one with state
    """
    snaps = ["snap_CA", "snap_TX", "snap_WI"]
    calendar = (
        calendar[snaps + ["d"]]
        .set_index("d")
        .rename(columns=lambda s: s[-2:])
        .rename_axis("state", axis=1)
        .stack()
        .rename("snap")
        .astype(np.int8)
        .reset_index()
        .pipe(categorify_ids, "pandas")
    )
    return pd.merge(sales, calendar, on=["d", "state"], how="left")


def future_dates(sales: pd.DataFrame, lags: list) -> pd.DataFrame:
    """
    Add future consumption values to each consumption period.
    """
    cols = {
        f"date_f{lag}": sales["date"] + pd.offsets.DateOffset(days=lag) for lag in lags
    }
    return sales.assign(**cols)


def max_date(source: str) -> pd.Timestamp:
    """
    Return the largest date depending on source.
    """
    return (
        pd.Timestamp("2016-05-22 00:00:00")
        if source == "evaluation"
        else pd.Timestamp("2016-04-24 00:00:00")
    )


def valid_fold(sales: pd.DataFrame, pipeline: str, source: str) -> pd.DataFrame:
    """
    Add the fold number of each date in the DataFrame
    """
    max_dt = max_date(source)
    return sales.assign(
        valid_fold=lambda x: ((max_dt - x["date_f28"]).dt.days // 28)
        .clip(upper=Config.KFOLD)
        .astype(np.int8)
    )


def future_features(sales: pd.DataFrame, features: list[str]) -> pd.DataFrame:
    """
    Add values of provided features as they will be at the horizon.
    """
    merge_columns = ["store", "item"]
    selected_columns = list(set(["date"] + merge_columns + features))
    updated_merge_columns = list(set(["date_f28"] + merge_columns) - {"date"})
    future_df = sales[selected_columns].rename(
        columns={c: f"{c}_f28" for c in ["date"] + features}
    )

    return pd.merge(
        sales,
        future_df,
        on=updated_merge_columns,
        how="left",
    )


def product_features(
    sales: pd.DataFrame, column_combos: Iterable[list[str]]
) -> pd.DataFrame:
    """
    Create new features as product of multiple ones.
    """
    for combo in column_combos:
        name = "__".join(["product"] + list(combo))
        sales = sales.assign(
            **{name: lambda df, combo=combo: df[list(combo)].product(axis=1)}
        )
    return sales
