"""
Code taken from : https://www.kaggle.com/c/m5-forecasting-accuracy/discussion/133834
"""
import importlib
import itertools

import numpy as np
import pandas as pd
from fastcore.utils import store_attr
from sklearn import metrics as skmetrics
from sklearn.utils import check_array

mae = skmetrics.mean_absolute_error


def compute_metrics(
    df: pd.DataFrame,
    truth_column: str,
    pred_column: str,
    metrics: list[tuple],
    prefix: str = "",
) -> dict[str, float]:
    """
    Compute a set of comparative metrics between the true and predicted value.
    """
    computed = {}
    modules = [skmetrics, importlib.import_module(__name__)]
    for metric, sample_weight in metrics:
        for mod in modules:
            fct = getattr(mod, metric, None)
            if fct:
                break
        else:
            raise KeyError(f"Unknown metric : {metric}")
        sample_weight = None if sample_weight is None else df[sample_weight]
        computed[prefix + metric] = fct(
            df[truth_column], df[pred_column], sample_weight=sample_weight
        )
    return computed


def rmse(truth, pred, sample_weight=None):
    """
    Root mean squared error.
    """
    return np.sqrt(
        skmetrics.mean_squared_error(truth, pred, sample_weight=sample_weight)
    )


def bias(truth, pred, sample_weight=None):
    truth, pred = (check_array(x, ensure_2d=False) for x in (truth, pred))
    if sample_weight is None:
        return (truth - pred).sum()
    sample_weight = check_array(sample_weight, ensure_2d=False)
    return np.dot(truth - pred, sample_weight).sum()


def mean_bias(truth, pred, sample_weight=None):
    return bias(truth, pred, sample_weight=sample_weight) / max(len(truth), 1)


def urmse(truth, preds, *, sample_weight=None) -> float:
    """
    RMSE without normalizing bu the sum of weights.
    """
    truth = check_array(truth, ensure_2d=False)
    preds = check_array(preds, ensure_2d=False)
    if sample_weight is None:
        sample_weight = 1
    else:
        sample_weight = check_array(sample_weight, ensure_2d=False)
    return np.sqrt((np.float_power(truth - preds, 2) * sample_weight).mean())


def mase(truth, preds, *, sample_weight=None):
    """
    Adaptation of rmsse for the absolute error case.
    """
    truth = check_array(truth, ensure_2d=False)
    preds = check_array(preds, ensure_2d=False)
    if sample_weight is None:
        sample_weight = 1
    else:
        sample_weight = check_array(sample_weight, ensure_2d=False)
    return (np.abs(truth - preds) * sample_weight).mean()


class WRMSSEEvaluator:
    """
    Evaluate the WRMSSEE score from M5 competition.
    See competition guide in references/ for metric description.
    """

    def __init__(self, groups, horizon):
        store_attr()
        self.groups = self.groups + ["all_id"]

    def fit(
        self,
        train_df: pd.DataFrame,
    ):
        sales_series = self._combined_series(train_df, "qty_f28")
        self.naive_errors_ = _naive_errors(sales_series)

        last_horizon = train_df[
            train_df.date > train_df.date.max() - pd.Timedelta(days=self.horizon)
        ]
        self.sales_ = self._combined_series(last_horizon.fillna({"ca": 0}), "ca").sum()

        self.scales_ = (
            self.sales_
            / self.sales_["all_id_0"]
            / len(self.groups)
            / self.naive_errors_
        )
        return self

    def _combined_series(self, df, target):
        df = df.assign(all_id=0)
        total = []
        for grp in self.groups:
            series = pd.pivot_table(
                df, index=["date"], columns=grp, values=target, aggfunc=np.nansum
            )
            series.columns = _flatten_multiindex_with_names(series.columns)

            total.append(series)
        return pd.concat(total, axis="columns")

    def score(self, df: pd.DataFrame):
        return self.series_error(df).sum()

    def combined_series(self, df: pd.DataFrame):
        truth = self._combined_series(df, "qty_f28")
        preds = self._combined_series(df, "est_qty")
        return truth, preds

    def series_bias(self, df: pd.DataFrame):
        truth, preds = self.combined_series(df)
        return truth - preds

    def series_error(self, df: pd.DataFrame):
        truth, preds = self.combined_series(df)
        return np.sqrt(np.float_power(preds - truth, 2).mean()) * self.scales_


def _naive_errors(train):
    train = train.where(train.cumsum() > 0)
    return (train - train.shift(1)).std()


def _flatten_multiindex_with_names(ind):
    if not isinstance(ind, pd.MultiIndex):
        return [(ind.name or "index") + "_" + str(x) for x in ind]
    components = [
        itertools.chain(*((ind.names[i], level) for i, level in enumerate(values)))
        for values in ind
    ]
    return ["_".join(map(str, col)) for col in components]
