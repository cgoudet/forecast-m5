import argparse

import joblib
import pandas as pd

from .. import DATADIR, MODELSDIR, SUBMITDIR
from ..data.submission import as_submission
from .processing import predictions_post_processing


def submit(source, model):
    """
    Apply a pickle and prepare a submission file.
    """
    model_config = joblib.load(MODELSDIR / f"{model}.joblib")
    print(model_config)

    fn = DATADIR / f"sales_{source}_submission.parquet"
    sales = pd.read_parquet(fn)

    sales["est_qte"] = model_config["model"].predict(sales[model_config["features"]])
    print(sales[["est_qte"]].describe())
    sales = predictions_post_processing(sales)
    print(sales[["est_qte"]].describe())

    fn = DATADIR / f"sales_{source}_submission.parquet"
    submission = as_submission(sales)
    submission.to_csv(SUBMITDIR / f"{model}.csv.zip")


def parse_args():

    parser = argparse.ArgumentParser(description="Create submission file for a model")
    parser.add_argument(
        "--source", type=str, default="validation", help="Dataset on which to apply"
    )
    parser.add_argument("--model", type=str, required=True, help="Model to evaluate")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    submit(args.source, args.model)
