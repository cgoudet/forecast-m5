import json

import neptune.new as neptune
import pandas as pd

from src.config import Config


def send_to_neptune(model_config: dict):
    model_config["text"]["features"] = "\n".join(model_config["features"])
    model_config["text"]["model_opt"] = json.dumps(model_config["model_opt"])

    run = neptune.init(
        project=f"{Config.NEPTUNE_USER}/forecast-m5-accuracy",
        api_token=Config.NEPTUNE_TOKEN,
    )

    to_save = ["name", "source", "git_hash"]
    for name in to_save:
        run[name] = model_config[name]

    model_config["n_features"] = len(model_config["features"])
    to_save_model = ["model_type", "n_features", "sample_weight"]
    for param in to_save_model:
        run[f"model/{param}"] = model_config[param]

    texts = model_config["text"]
    for name, content in texts.items():
        run[f"artifacts/{name}"] = content

    artifacts = model_config["artifacts"]
    for name, obj in artifacts.items():
        run[f"artifacts/{name}"].upload(obj)

    save_metrics(run, model_config["metrics"])
    run.stop()


def add_dataset_params(model_config: dict, frame: pd.DataFrame) -> dict:
    params = {
        "size": len(frame),
    }
    return {**model_config, "dataset_params": params}


def save_metrics(run, metrics):
    for fold_metrics in metrics:
        fold = fold_metrics["fold"]
        for name, value in fold_metrics.items():
            if name == "fold":
                continue
            run[f"fold{fold}/{name}"] = value

    avg = pd.DataFrame(metrics).drop(columns=["fold"]).mean().to_dict()
    for name, value in avg.items():
        run[f"avg/{name}"] = value
