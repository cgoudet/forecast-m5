import pandas as pd

from src.features.features import ZERO_DAYS


def predictions_post_processing(
    sales: pd.DataFrame, column: str = "est_qty"
) -> pd.DataFrame:
    corrected = (
        sales[column]
        .where(~sales["date_f28"].isin(ZERO_DAYS) & (sales["price_f28"] > 0), 0)
        .clip(lower=0)
    )
    return sales.assign(**{column: corrected})
