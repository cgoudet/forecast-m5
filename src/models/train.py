import json
import time
from pathlib import Path

import click
import joblib
import pandas as pd
import structlog
from cgoudetcore.learning import fit_params, init_model, pred_params
from cgoudetcore.utils import git_hash

from src import DATADIR, ENCODIR, MODELSDIR
from src.features import CATEGORIES
from src.features.features import ZERO_DAYS
from src.models.metrics import compute_metrics
from src.models.processing import predictions_post_processing
from src.models.tracking import add_dataset_params, send_to_neptune

logger = structlog.get_logger()


@click.command()
@click.argument("model_name")
@click.option("--save", default=False, is_flag=True, help="Save the pickle locally")
@click.option("--track", default=False, is_flag=True, help="Save to neptune.")
@click.option(
    "--configdir",
    default=str(MODELSDIR),
    type=str,
    help="Directory of configuration files",
)
def main(model_name, save, track, configdir):
    train_model(model_name, save, Path(configdir), track)


def train_model(model_name: str, save: bool, configdir: Path, track):
    """
    Apply a pickle and prepare a submission file.
    """
    configdir = Path(configdir)
    model_name = Path(model_name).stem
    with open(configdir / f"{model_name}.json") as f:
        model_config = json.load(f)
    features = model_config["features"]
    sample_weight = model_config.get("sample_weight")
    model_config["git_hash"] = git_hash()
    model_config["text"] = {}
    model_config["artifacts"] = {}

    sales = (
        pd.read_parquet(DATADIR / f"{model_config['source']}_final_features.parquet")
        .assign(est_qty=0)
        .dropna(subset=features + ["qty_f28"])
    )
    model_config = add_dataset_params(model_config, sales)

    common_mask = ~sales.date.isin(ZERO_DAYS)
    scores = []
    max_fold = sales["valid_fold"].max()
    for fold in range(max_fold - 1, -1, -1):
        metrics = {"fold": fold}

        mask_train = (
            common_mask & (sales["valid_fold"] > fold) & (sales["keep_train"] > 0)
        )
        X_train = sales.loc[mask_train, features]
        y_train = sales.loc[mask_train, "qty_f28"]
        weight_train = (
            None if sample_weight is None else sales.loc[mask_train, sample_weight]
        )

        mask_valid = common_mask & (sales["valid_fold"] == fold)
        X_valid = sales.loc[mask_valid, features]
        y_valid = sales.loc[mask_valid, "qty_f28"]
        weight_valid = (
            None if sample_weight is None else sales.loc[mask_valid, sample_weight]
        )

        evaluator = joblib.load(
            ENCODIR / f"evaluator_{model_config['source']}_fold{fold}.joblib"
        )

        reg = init_model(model_config["model_type"], model_config.get("model_opt", {}))
        start = time.time()
        reg.fit(
            X_train,
            y_train,
            sample_weight=weight_train,
            **fit_params(
                features,
                reg,
                X_valid,
                y_valid,
                w_valid=weight_valid,
                categories=CATEGORIES,
            ),
        )
        metrics["time"] = time.time() - start
        start = time.time()
        sales = predictions_post_processing(
            sales.assign(
                est_qty=lambda df: reg.predict(df[features], **pred_params(reg))
            )
        )

        metrics.update(
            compute_metrics(
                sales[sales["valid_fold"] == fold],
                "qty_f28",
                "est_qty",
                metrics=model_config.get("def_metrics", []),
                prefix="valid_",
            )
        )
        metrics.update(
            compute_metrics(
                sales[sales["valid_fold"] > fold],
                "qty_f28",
                "est_qty",
                metrics=model_config.get("def_metrics", []),
                prefix="train_",
            )
        )

        metrics["valid_wrmsse"] = evaluator.score(sales[sales["valid_fold"] == fold])

        sales = predictions_post_processing(sales.assign(est_qty=lambda df: df["qty"]))
        lagged_score = evaluator.score(sales[sales["valid_fold"] == fold])
        metrics["valid_improvement"] = metrics["valid_wrmsse"] / lagged_score

        logger.info("metrics", **metrics)
        scores.append(metrics)

    model_config["text"]["perf_sku"] = (
        evaluator.series_error(sales[sales["valid_fold"] == 0])
        .sort_values(ascending=False)
        .to_frame("wrmsse")
        .to_csv()
    )

    model_config["metrics"] = scores
    print(pd.DataFrame(scores))
    print(pd.DataFrame(scores).mean())
    if track:
        send_to_neptune(model_config)


if __name__ == "__main__":
    main()
