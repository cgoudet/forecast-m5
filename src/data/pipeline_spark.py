from itertools import chain
from typing import Optional

from cgoudetcore.utils import diffl
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.functions import col, create_map, explode, lit, regexp_extract

from src import DATADIR
from src.config import Config
from src.data.categories import categorify_ids
from src.features.features import (
    future_dates,
    future_features,
    merge_date_infos,
    merge_prices,
    valid_fold,
)


def pipe(self, func, *args, **kwargs):
    return func(self, *args, **kwargs)


DataFrame.pipe = pipe


def spark_context():
    return (
        SparkSession.builder.master("spark://spark-master:7077")
        .appName("forecast-m5")
        .config("spark.default.parallelism", 20)
        .config("spark.executor.memory", "512M")
        .getOrCreate()
    )


def _product_mapping(df):
    cats = {cat: i for i, cat in enumerate(Config.CATS)}
    mapping_cats = create_map([lit(x) for x in chain(*cats.items())])
    return (
        df.withColumn("cat", mapping_cats[col("cat")].cast("tinyint"))
        .withColumn(
            "dept",
            (
                regexp_extract("dept", "[A-Z]*_([0-9])", 1).cast("int")
                + col("cat") * 10
            ).cast("tinyint"),
        )
        .withColumn(
            "item",
            (
                regexp_extract("item", ".*_([0-9]{3,})$", 1).cast("int")
                + col("dept") * 1000
            ).cast("short"),
        )
    )


def melt(
    df: DataFrame,
    id_vars: list,
    value_vars: Optional[list] = None,
    var_name: str = "variable",
    value_name: str = "value",
) -> DataFrame:
    """Convert :class:`DataFrame` from wide to long format."""

    if value_vars is None:
        value_vars = diffl(df.columns, id_vars)

    # Create map<key: value>
    _vars_and_vals = create_map(
        list(chain.from_iterable([[lit(c), col(c)] for c in value_vars]))
    )

    _tmp = (
        df.select(*id_vars, explode(_vars_and_vals))
        .withColumnRenamed("key", var_name)
        .withColumnRenamed("value", value_name)
    )

    return _tmp


def sales_melted(context, source: str, **kwargs):
    """
    Transform a dataset from having days in columns to days in rows and adds raw additionnal data :
    - Make categories to integer
    - Add snap features flags
    - Add date related events
    - Add the date at horizon
    - Add price
    """
    ids = ["item_id", "dept_id", "cat_id", "store_id", "state_id"]
    fn = DATADIR / f"sales_train_{source}.csv"
    sales = context.read.options(header="True", delimiter=",").csv(str(fn)).drop("id")
    for c in ids:
        sales = sales.withColumnRenamed(c, c[:-3])
    return (
        sales.pipe(categorify_ids, "spark")
        .pipe(
            melt,
            id_vars=[c for c in sales.columns if not c.startswith("d_")],
            var_name="d",
            value_name="qty",
        )
        .withColumn("qty", col("qty").cast("int"))
        .pipe(merge_date_infos, context)
        .pipe(merge_prices, context)
    )


def base_features(sales, source, **kwargs):
    return (
        sales.pipe(future_dates, [28])
        .pipe(valid_fold, pipeline="spark", source=source)
        .withColumn("ca", col("price") * col("qty"))
        .withColumn("keep_agg", lit(1))
        .withColumn("keep_train", lit(1))
    )


def final_features(sales, **kwargs):
    return sales.pipe(future_features, Config.FUTURE_FEATURES)
