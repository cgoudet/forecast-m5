import sys
from dataclasses import dataclass
from pathlib import Path

import joblib
import numpy as np
import pandas as pd

import src.data.pipeline_pd as pipepd
import src.data.pipeline_spark as pipespark
from src import DATADIR, ENCODIR
from src.config import Config
from src.data.submission import as_submission
from src.models.metrics import WRMSSEEvaluator


def weekly_prices(*args, **kwargs):
    """
    Format raw data from products weekly prices.
    """
    price_fn = DATADIR / "sell_prices.csv"
    prices = (
        pd.read_csv(
            price_fn,
            dtype={
                "wm_yr_wk": np.int16,
                "sell_price": np.float32,
                "item_id": str,
                "store_id": str,
            },
        )
        .rename(columns={"store_id": "store", "item_id": "item", "sell_price": "price"})
        .pipe(pipepd.categorify_ids)
    )
    prices.to_parquet(DATADIR / "weekly_prices.parquet")


def create_small_subset(num_products: int = 500, **kwargs):
    """
    Generate a subset of the full dataset to make fast iterations.
    """
    sales = pd.read_csv(DATADIR / "sales_train_validation.csv")

    np.random.seed(876)
    subset_items = np.random.permutation(sorted(sales.item_id.unique()))[:num_products]
    sales[sales.item_id.isin(subset_items)].to_csv(
        DATADIR / "sales_train_small.csv", index=False
    )


def save_parquet(sales: pd.DataFrame, fn: Path):
    if isinstance(sales, pd.DataFrame):
        sales.to_parquet(fn)
    else:
        sales.write.mode("overwrite").parquet(str(fn))


def train_evaluators(source: str, **kwargs):
    fn = DATADIR / f"{source}_final_features.parquet"
    to_read = [
        "item",
        "dept",
        "cat",
        "store",
        "state",
        "qty",
        "qty_f28",
        "date",
        "price",
        "ca",
        "valid_fold",
    ]
    groups = [
        "state",
        "store",
        "cat",
        "dept",
        ["state", "cat"],
        ["state", "dept"],
        ["store", "cat"],
        ["store", "dept"],
        ["item"],
        ["item", "state"],
        ["item", "store"],
    ]
    sales = (
        pd.read_parquet(fn, columns=to_read)
        .assign(est_qty=lambda df: df["qty"])
        .dropna(subset=["qty_f28"])
    )
    max_fold = sales["valid_fold"].max()
    for fold in range(max_fold - 1, -1, -1):
        train = sales[sales["valid_fold"] > fold]
        valid = sales[sales["valid_fold"] == fold]

        evaluator_fn = ENCODIR / f"evaluator_{source}_fold{fold}.joblib"
        print(evaluator_fn.stem)
        evaluator = WRMSSEEvaluator(groups=groups, horizon=28).fit(train)
        print(evaluator.score(valid))
        joblib.dump(evaluator, evaluator_fn)


def select_submission(sales, **kwargs):
    return sales[sales["qty_f28"].isnull()]


def run_named_fct(fct_name: str, source: str, **kwargs):
    mods = {"pandas": pipepd, "spark": pipespark}
    pipeline = "pandas" if source == "small" else Config.SOURCE_PIPELINE_VALIDATION
    current = sys.modules[__name__]
    return getattr(mods[pipeline], fct_name, getattr(current, fct_name, None))(
        pipeline=pipeline, source=source, **kwargs
    )


def submit(sales, source, **kwargs):
    sales = (
        pd.read_parquet(DATADIR / f"{source}_select_submission.parquet")
        .assign(est_qte=lambda df: df["qty"])
        .pipe(as_submission, source=source)
    )
    sales.to_csv(DATADIR / f"{source}_submit.csv")


@dataclass
class Step:
    """
    Generic class to define a pipeline step
    """

    name: str
    read: str
    save: bool


PIPELINE = [
    Step(name="weekly_prices", read=None, save=True),
    Step(name="create_small_subset", read=None, save=True),
    Step(name="sales_melted", read=None, save=True),
    Step(name="base_features", read="sales_melted", save=True),
    Step(name="train_encodings", read="base_features", save=True),
    Step(name="merge_encodings", read="base_features", save=True),
    Step(name="final_features", read="merge_encodings", save=True),
    Step(name="select_submission", read="final_features", save=True),
    Step(name="train_evaluators", read="final_features", save=False),
    Step(name="submit", read="select_submission", save=False),
]
