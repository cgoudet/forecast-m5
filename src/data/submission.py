import pandas as pd

from src import DATADIR
from src.data.categories import uncategorify_ids


def as_submission(sales, source):
    submission = (
        sales[["item", "store", "est_qte", "date"]]
        .set_index(["item", "store", "date"])["est_qte"]
        .unstack(level=-1)
        .pipe(_rename_columns)
        .reset_index()
        .pipe(uncategorify_ids, "pandas")
        .assign(
            id=lambda df: df["item"].str.cat(
                df["store"] + "_" + ("validation" if source == "small" else source),
                sep="_",
            )
        )
        .drop(columns=["item", "store"])
        .set_index("id")
    )
    return _align_to_reference(submission)


def _rename_columns(submission):
    return submission.reindex(sorted(submission.columns), axis=1).set_axis(
        pd.Index([f"F{i+1}" for i in range(len(submission.columns))]), axis=1
    )


def _align_to_reference(submission):

    sample_fn = DATADIR / "sample_submission.csv"
    sample = pd.read_csv(sample_fn).set_index("id")
    return submission.align(sample)[0].fillna(0).astype(int)
