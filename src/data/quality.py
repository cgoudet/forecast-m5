from functools import reduce

import pandas as pd


def drop_bad_quality(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Remove handpicked data
    """
    masks = [
        # (sales["store"] == 0) & (sales["item"] == 1394) & (sales["qte"] > 80),
    ]
    mask = reduce(lambda x, y: x | y, masks)
    return sales[~mask]
