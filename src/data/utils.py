import dask.dataframe as dd
import pandas as pd


def read_parquet(source, fn, **kwargs):
    if dask_pipeline(source):
        return dd.read_parquet(fn, **kwargs)
    return pd.read_parquet(fn)


def concat(source, objs, **kwargs):
    if dask_pipeline(source):
        return dd.concat(objs, **kwargs)
    return pd.concat(objs, **kwargs)


def dask_pipeline(source):
    return source in ["validation", "evaluation", "dasksmall"]


def frame(source):
    return dd.DataFrame if dask_pipeline(source) else pd.DataFrame
