from collections import Counter

import numpy as np
import pandas as pd
import structlog
from cgoudetcore.utils import reduce_float_type

from src import DATADIR, ENCODIR
from src.config import Config
from src.data.categories import categorify_ids
from src.data.quality import drop_bad_quality
from src.features.embeddings import Embedding, item_snap_shift, learn_merge_embedding
from src.features.features import (
    add_datepart,
    combined_features,
    future_dates,
    future_features,
    max_date,
    merge_date_infos,
    merge_prices,
    valid_fold,
)
from src.features.rolling import rolling_features

pd.options.mode.chained_assignment = None
logger = structlog.get_logger()


def sales_melted(source: str, pipeline, **kwargs):
    """
    Transform a dataset from having days in columns to days in rows and adds raw additionnal data :
    - Make categories to integer
    - Add snap features flags
    - Add date related events
    - Add the date at horizon
    - Add price
    """
    fn = DATADIR / f"sales_train_{source}.csv"
    ids = ["item_id", "dept_id", "cat_id", "store_id", "state_id"]
    sales = pd.read_csv(fn).rename(columns={c: c[:-3] for c in ids}).drop(columns="id")
    return (
        sales.pipe(categorify_ids, pipeline)
        .melt(id_vars=[c for c in sales.columns if not c.startswith("d_")])
        .rename(columns={"variable": "d", "value": "qty"})
        .astype({"qty": np.int16})
        .pipe(merge_date_infos)
        .pipe(future_dates, [28])
        .pipe(future_features, ["wm_yr_wk"])
        .pipe(merge_prices)
        .pipe(reduce_float_type)
    )


def base_features(sales: pd.DataFrame, source: str, **kwargs) -> pd.DataFrame:
    out = (
        sales.pipe(valid_fold, pipeline="pandas", source=source)
        .pipe(add_datepart)
        .pipe(drop_non_active, source=source)
        .assign(
            ca=lambda df: np.float32(df["price"] * df["qty"]),
            keep_agg=np.int8(1),
            keep_train=np.int8(1),
        )
        .pipe(reduce_float_type)
    )
    return out


def drop_non_active(sales: pd.DataFrame, source: str) -> pd.DataFrame:
    """
    Remove data points where prediction is not meaningful.
    """
    max_dt = max_date(source)
    return sales[(sales["price_f28"] > 0) | (sales["date_f28"] > max_dt)]


def train_encodings(sales, source, force=False, **kwargs):
    rolling_features(
        sales,
        configs=Config.ROLLING_CONFIG,
        rolling_reference="date",
        savedir=ENCODIR,
        force=force,
        source=source,
    )


def clean_bad_quality(source, force=False):
    fn = DATADIR / f"sales_{source}_formatted.parquet"
    sales = pd.read_parquet(fn).pipe(drop_bad_quality)
    outfn = DATADIR / f"sales_{source}_cleaned.parquet"
    sales.to_parquet(outfn)


def merge_encodings(sales, source, **kwargs):
    encodings_fn = sorted(ENCODIR.glob(f"encodings_{source}_*.parquet"))
    encodings = [pd.read_parquet(fn) for fn in encodings_fn]
    merged = pd.concat([sales] + encodings, axis=1)
    _assert_unique_columns(merged)
    return merged


def _assert_unique_columns(df: pd.DataFrame):
    duplicates = [
        column for column, count in Counter(df.columns).most_common() if count > 1
    ]
    if duplicates:
        raise RuntimeError("Duplicate columns : {}".format(", ".join(duplicates)))


def final_features(sales, source: str, force: bool, **kwargs):
    complex_embeddings = [Embedding(fct=item_snap_shift)]
    for emb in complex_embeddings:
        sales = sales.pipe(learn_merge_embedding, emb, source=source, force=force)
    return (
        sales.pipe(future_features, Config.FUTURE_FEATURES)
        .pipe(combined_features)
        .pipe(reduce_float_type)
    )
