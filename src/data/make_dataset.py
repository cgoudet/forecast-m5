import time

import click
import pandas as pd
import structlog
from pyspark.sql import DataFrame

from src import DATADIR
from src.config import Config
from src.data.pipeline import PIPELINE, run_named_fct, save_parquet
from src.data.pipeline_spark import spark_context

logger = structlog.get_logger()
_NON_SPARK = ["create_small_subset", "train_evaluators", "weekly_prices", "submit"]
_STEPS = PIPELINE


@click.command()
@click.option(
    "--start-step",
    type=click.Choice([s.name for s in _STEPS]),
    default="sales_melted",
    help="Frist step to compute",
)
@click.option(
    "--end-step",
    type=click.Choice([s.name for s in _STEPS]),
    default="select_submission",
    help="Last step to compute",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Recompute stateful objects. Stateless objects are always recomputed.",
)
@click.option(
    "--source",
    type=click.Choice(["small", "validation", "evaluation"]),
    default="small",
    help="Which learning source must be executed",
)
def run_pipeline(
    start_step: str,
    end_step: str,
    force: bool,
    source: str,
):
    steps = get_steps(start_step, end_step, _STEPS)

    for step in steps:
        run_step(step, source, force)


def get_steps(start, stop, steps):
    names = [s.name for s in steps]
    index_start = names.index(start)
    return steps[index_start : max(names.index(stop), index_start) + 1]


def run_step(step, source, force):
    context = None
    if (
        source == "validation"
        and step.name not in _NON_SPARK
        and Config.SOURCE_PIPELINE_VALIDATION == "spark"
    ):
        context = spark_context()
    start = time.time()
    sales = read_input(step.read, source, context)
    sales = run_named_fct(
        step.name, source=source, sales=sales, force=force, context=context
    )

    if step.name == "add_final_features" and context:
        sales.coalesce(1)

    if isinstance(sales, (DataFrame, pd.DataFrame)) and step.save:
        save_parquet(sales, DATADIR / f"{source}_{step.name}.parquet")
    print("time", step.name, time.time() - start)
    if context:
        context.stop()


def read_input(name, source: str, context=None):
    if not name:
        return

    fn = DATADIR / f"{source}_{name}.parquet"
    if source == "small" or Config.SOURCE_PIPELINE_VALIDATION == "pandas":
        return pd.read_parquet(fn)
    elif source == "validation":
        return context.read.parquet(str(fn))


if __name__ == "__main__":  # pragma: no cover
    run_pipeline()
