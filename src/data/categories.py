import numpy as np
import pandas as pd


class IntEncoder:
    def __init__(self, pipeline: str):
        """
        Generic encoder from categories to integer representation.

        :param pipeline:
        Wether to use 'pandas' or 'spark' implementation of the transformation.
        """
        self.fct_match_ = {"pandas": (self.pd_encode, self.pd_decode)}
        assert pipeline in self.fct_match_.keys()
        self.pipeline = pipeline

    def encode(self, serie):
        return self.fct_match_[self.pipeline][0](serie)

    def decode(self, serie):
        return self.fct_match_[self.pipeline][1](serie)

    def pd_encode(self, serie):
        raise NotImplementedError()

    def pd_decode(self, serie):
        raise NotImplementedError()


class State(IntEncoder):
    map_ = ["CA", "TX", "WI"]

    def pd_encode(self, serie):
        states = {state: i for i, state in enumerate(self.map_)}
        return serie.map(states).astype(np.int8)

    def pd_decode(self, serie):
        states = {i: state for i, state in enumerate(self.map_)}
        return serie.map(states).astype(str)


class Cat(IntEncoder):
    map_ = ["FOODS", "HOBBIES", "HOUSEHOLD"]

    def pd_encode(self, serie):
        match = {value: i for i, value in enumerate(self.map_)}
        return serie.map(match).astype(np.int8)

    def pd_decode(self, serie):
        match = {i: value for i, value in enumerate(self.map_)}
        return serie.map(match).astype(str)


class Store(IntEncoder):
    def pd_encode(self, serie):
        extracted = serie.str.extract("^(.*)_([0-9])$")
        states = State(self.pipeline).encode(extracted[0])
        return pd.Series(np.int8(extracted[1].astype("int") + states * 10))

    def pd_decode(self, serie):
        states = State(self.pipeline).decode(serie // 10)
        stores = serie % 10
        return states.str.cat(stores.astype(str), sep="_")


class Dept(IntEncoder):
    def pd_encode(self, serie):
        extracted = serie.str.extract("^(.*)_([0-9])$")
        states = Cat(self.pipeline).encode(extracted[0])
        return pd.Series(np.int8(extracted[1].astype("int") + states * 10))

    def pd_decode(self, serie):
        states = Cat(self.pipeline).decode(serie // 10)
        stores = serie % 10
        return states.str.cat(stores.astype(str), sep="_")


class Item(IntEncoder):
    def pd_encode(self, serie):
        extracted = serie.str.extract("^(.*)_([0-9]{3,})$")
        deps = Dept(self.pipeline).encode(extracted[0])
        return pd.Series(np.int16(deps * 1000 + extracted[1].astype(int)))

    def pd_decode(self, serie):
        depts = Dept(self.pipeline).decode(serie // 1000)
        items = serie % 1000
        return depts.str.cat(items.astype(str).str.zfill(3), sep="_")


def categorify_ids(df: pd.DataFrame, pipeline: str) -> pd.DataFrame:
    """
    Transform specific object categories into integer.
    """
    matching = {
        "state": State,
        "cat": Cat,
        "store": Store,
        "dept": Dept,
        "item": Item,
    }
    cols = {
        k: cls(pipeline).encode(df[k]) for k, cls in matching.items() if k in df.columns
    }
    return df.assign(**cols)


def uncategorify_ids(df: pd.DataFrame, pipeline: str) -> pd.DataFrame:
    """
    Transform integer categories back into their string representation
    """
    matching = {
        "state": State,
        "cat": Cat,
        "store": Store,
        "dept": Dept,
        "item": Item,
    }
    cols = {
        k: cls(pipeline).decode(df[k]) for k, cls in matching.items() if k in df.columns
    }
    return df.assign(**cols)
