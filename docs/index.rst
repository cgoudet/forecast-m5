.. forecast-m5 documentation master file, created by
   sphinx-quickstart on Mon Nov  1 13:46:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to forecast-m5's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   data/index.rst
   features/index.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
