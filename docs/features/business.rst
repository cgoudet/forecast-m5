Business features
******************

This page describes features specific to this dataset.

.. contents::

Snap
===============

"The United States federal government provides a nutrition assistance benefit called the Supplement Nutrition Assistance Program (SNAP). SNAP provides low income families and individuals with an Electronic Benefits Transfer debit card to purchase food products. In many states, the monetary benefits are dispersed to people across 10 days of the month and on each of these days 1/10 of the people will receive the benefit on their card."

SNAP is provided in the dataset as a per-state binary variable.
A selection of features is created in order to leverage this information is a way most effective with decision trees.

- **snap_f28** : a simple flag wether the predicted day will be a snap day.
- **item_snap** : item id positive if future snap else a negative id.
- **product_snap_rolling_sum** : product between future snap and the rolling sum of sales the last 28 days for each (item x store).
- **product_snap_rolling_dayofweek** : product between future snap and median predictor.
- **item_snap_shift** : a feature that attribute to each item the difference between average bias of a simple median prediction with or without snap.
- **item_snap_flag_shift** : item_snap_shift if snap else an arbitrary value.

.. csv-table:: Performances
   :file: images/results_test_snap.csv
   :header-rows: 1
