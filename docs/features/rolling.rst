Rolling features
****************

.. contents::

Rolling features consist in rolling statistics of the sales over various period of time.
These features are not directly incorporated into the predictions as they are not available for all time steps in the test set.
Instead, their value 28 days earlier is used.

The initial set of features consist in all possible combinations of aggreations over:

- different target values : `ca` or `qte`
- different windows : 7 or 28 days
- different aggregation functions : mean, std, median, dayofweek_mean, dayofweek_std, zero_rate, sum
- different hierarchies : store, item, store_item, store_dept, store_cat, state_item, state_dept, state_cat

An additional rolling feature present in most hierarchies is the rolling naive error, defined as the mean squared error of sales compared between one day and the next.

5 additional features are generated, consisting in the average sales of this particular day of the week over the last 2 months, over the total sales other the same period.

In total, 145 features are generated.
Most of these variables will be useless in the modelisation.
We need to identify the most useful ones.

Correlation reduction
=====================

We try to identify features which are duplicates of one another.
We first compute the spearman correlation of all these variables.
Then we identify correlated features with a DBSCAN, where the distance matrix $1-abs(corr)$ is used, with an $eps=0.15$.
Finally a notion of order between features allows to select the most pertinent feature within a cluster.

To accelerate the process, it is performed first within a single hierachical encodings.
Then the remaining features are compared to each other.
This remove 69 features.
