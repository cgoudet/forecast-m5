UMAP Embeddings
***************

Embeddings are strategies to encode categorical variable to numerical variables.
Here, the strategy relies on creating multiple features for each categorical value and to apply dimension reduction (using umap) in order to create low dimension embedding for the category.

.. contents::

Strategy
========

All the embeddings proposed in this project are based on the same strategy, applied at different level of the problem hierarchy.

Firstly, create a pivot table with the categorical variable and the week as index.
The combination of the two is made to consider the weekly fluctuations and increase the coverage of the phase space for each categorical value.
The columns of the pivot table are the aggregated sales (or dollar amount) as a function of other categories.
For example, on can compute the amount items sold per day of week (columns) as a function of the store (index).

Once the pivot table is created, normalise it -per line- in order to get the relative amount of sales per column for each lines.
This allows to compare each line based on their distribution instead of raw values.
At this point, we get a high dimension embedding of each line.

Then, apply UMAP dimension reduction algorithm to reduce the embedding to few dimension.

Finally, make sure to not use the embedding with the current week/day on a data point but the one from 28 days earlier to avoid data leakage.


Applications
============

store_dayofweek (6a5cdd9c)
---------------------------

This application aims at creating a mapping at the (store, week) level.
For each store, one look at the relative sales per day of week.
The following embedding is created.

.. image:: images/umap_embedding_store_dayofweek_init.png

The vast majority of stores end up in the large expected continuum but small groups of store are clustered.
A deeper look into those small clusters show that they are related to public holidays.
All stores are closed on Christmas.
This means that all stores have a 0% sales on this day for this week; which is very different from a standard week where any day should have around 10% of the total weekly sales.

Because holidays are already taken care of in other variables, those embeddings are replace by the average of the store within the main cluster.
This embedding has been tested to replace the store id against a simple model (id 6a5cdd9c)
A cross validation has shown a reduction of performance due to this swap.
Shap technique has identified no significant impact of those embeddings.
As a result, those embeddings are removed from the pipeline.

Performances:

- baseline (id-small-lgbreg) : 2.046
- 6a5cdd9c : 2.059

store_item (83d3f038)
-------------------------
This application create an embedding where each store (per week) is characterised by the relative amount of sales of each item.
Due to the large dimension of the feature space, it was decided to keep up to 3 dimensions for the final embedding.
The embeddings seem to fairly separate each store, coupled with the notion of time that shape the data in length.

.. image:: images/umap_embedding_store_item.png

These variables show a small improvement of performances.

Performances:

- baseline (id-small-lgbreg) : 2.046
- 83d3f038 : 2.039
