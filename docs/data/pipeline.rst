Data pipeline
******************

.. contents::

**weekly_price**
Format the weekly price of products into a parquet file with the correct column names.

**create_small_subset**
Generate a subset of the full dataset to make fast iterations.

**sales_melted**
Transform the raw dataset into a (product x store x day) DataFrame and add basic raw data.
- Make categories to integer
- Add snap features flags
- Add date related events
- Add the date at horizon
- Add price

**base_features**
Add basic independent features to the DataFrame.
- add valid_fold
- dates features (dayofweek, ...)
- Remove non meaningful prediction points
- Add ca, kee_train, keep_agg
